using System;
using System.Linq;
namespace Array {
  public class MyMethods {
    public static void SwitchMethod () {
      Console.WriteLine ("---Welcome to Array task! \n 1.Compute the Sum of Elements, \n 2.Larger Value between First element and Last Element, \n 3.Check Array Contains Odd Number, \n 4.2D array,----------");
      Console.WriteLine ("Choose Which task Would you like to Run from the Above Tasks:");
      int taskNumber = Convert.ToInt32 (Console.ReadLine ());
      switch (taskNumber) {
        case 1:
          sumOfTheArray ();
          break;
        case 2:
          largestValueinArray ();
          break;
        case 3:
          CheckOddNuminArray ();
          break;
        case 4:
          Array2D();
          break;
        default:
          Console.WriteLine ("Error you have given a invalid task number as input");
          break;
      }
      Console.WriteLine ("Would you like to do String task again (Y/N)?:");
      string TaskYesOrNo = Console.ReadLine ().ToUpper ();
      if (TaskYesOrNo == "Y") {
        SwitchLoop (TaskYesOrNo);
      }
      TaskYesOrNo = "";
    }
    public static void SwitchLoop (string TaskYesOrNo) {
      while (TaskYesOrNo == "Y") {
        SwitchMethod ();
        TaskYesOrNo = "";
      }
    }
    public static void sumOfTheArray () {
      int sum = 0;
      int[] array = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
      sum = array.Sum ();
      Console.WriteLine ("------Task-1 Sum of the Array----");
      Console.WriteLine ("Sum of this Array:[{1,2,3,4,5,6,7,8,9}]");
      Console.WriteLine ("Output:");
      Console.WriteLine (sum);
      Console.WriteLine ("--------------------------------------");
    }

    public static void largestValueinArray () {
      int value = 0;
      int[] array = { 1, 2, 300, 4, 5, 6, 7, 8, 9, 100 };
      Console.WriteLine ("------Task-2 Find Largest Value between 1st and last Element of the Array----");
      Console.WriteLine ("Output:");
      value = array.Max ();
      Console.WriteLine ("The largest value between 1st and last Element of the Array is: " + value);
      Console.WriteLine ("--------------------------------------");

    }
    public static void CheckOddNuminArray () {
      int[] array = { 1, 2, 300, 4, 5, 6, 7, 8, 9, 100 };
      Console.WriteLine ("------Task-3 Check any odd numbers are present in the Array----");
      Console.WriteLine ("Output:");
      foreach (var item in array) {
        if (item % 2 != 0) {
          Console.WriteLine ("True");
          break;
        } else {
          Console.WriteLine ("False");
        }
      }
      Console.WriteLine ("--------------------------------------");
    }

    public static void Array2D () {
      int i, j;
      int[, ] arr1 = new int[3, 3];

      Console.WriteLine ("-------Task-4 Read a 2D array of size 3x3 and print the matrix:");
      Console.WriteLine ("Input elements in the matrix :");
      for (i = 0; i < 3; i++) {
        for (j = 0; j < 3; j++) {
          Console.Write("element - [{0},{1}] : \n", i, j);
          arr1[i, j] = Convert.ToInt32 (Console.ReadLine ());
        }
      }

      Console.WriteLine ("The matrix is :");
      for (i = 0; i < 3; i++) {
        Console.Write ("\n");
        for (j = 0; j < 3; j++)
          Console.Write ("{0}\t", arr1[i, j]);
      }
      Console.Write ("\n\n");
      Console.WriteLine ("--------------------------------------");
    }
  }
}